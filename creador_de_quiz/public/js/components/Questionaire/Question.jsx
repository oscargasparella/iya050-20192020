const { React } = window;
const { useContext, useState, useEffect } = React;
const { QuestionaireContext } = window;

const QUESTION_TIME = 30; //seconds
const ONE_SECOND = 1000; //ms

const Question = ({ match }) => {
  const [timeLeft, setTimeLeft] = useState(QUESTION_TIME);
  const { questions, onAnswer } = useContext(QuestionaireContext);

  const currentQuestion = questions.find(
    (q) => q.id === Number(match.params.id)
  );

  function handleAnswer(answer) {
    onAnswer(currentQuestion, answer);
  }

  function handleNoAnswer() {
    const blankAnswer = {
      type: 'Answer',
      id: `blank-${currentQuestion.id}`,
      text: '',
      correct: false,
    };

    onAnswer(currentQuestion, blankAnswer);
  }

  useEffect(() => {
    if (timeLeft > 0) {
      const timeout = setTimeout(() => {
        const timeElapsed = 1; //seconds
        setTimeLeft((timeLeft) => timeLeft - timeElapsed);
      }, ONE_SECOND);

      return () => {
        clearTimeout(timeout);
      };
    }

    handleNoAnswer();
  }, [timeLeft]);

  useEffect(() => {
    setTimeLeft(QUESTION_TIME);
  }, [currentQuestion]);

  return (
    <React.Fragment>
      <header className="header">
        <h1 className="header__title">{currentQuestion.text}</h1>
        <h3 className="header__subtitle">Time Left: {timeLeft}</h3>
      </header>

      <section className="container">
        <ul className="option-list">
          {currentQuestion.answers.map((answer, i) => (
            <li className="option-list__item" key={answer.id}>
              <button
                className="button button--link"
                onClick={() => handleAnswer(answer)}
              >
                {`${i + 1} - ${answer.text}`}
              </button>
            </li>
          ))}
        </ul>
      </section>
    </React.Fragment>
  );
};

window.Question = Question;
