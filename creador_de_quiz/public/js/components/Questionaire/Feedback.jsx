const { React } = window;
const { Fragment, useContext } = React;
const { Link } = window.ReactRouterDOM;
const { QuestionaireContext } = window;

const Feedback = () => {
  const { name, questions, answers } = useContext(QuestionaireContext);

  const correctAnswers = answers.filter(({ correct }) => correct);
  const score = ((correctAnswers.length / answers.length) * 100).toFixed(2);

  return (
    <Fragment>
      <header className="header">
        <h1 className="header__title">{name} Results</h1>
      </header>
      <section className="container">
        <table className="widget">
          <thead>
            <tr className="widget-header">
              <th className="widget__message">Question</th>
              <th className="widget__message">Answer</th>
              <th className="widget__message">Correct</th>
            </tr>
          </thead>
          <tbody>
            {answers.map((answer, i) => (
              <tr key={answer.id}>
                <td className="widget__message">{questions[i].text}</td>
                <td className="widget__message">{answer.text}</td>
                <td className="widget__message">
                  {answer.correct ? 'Yes' : 'No'}
                </td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr className="widget-header">
              <th className="widget__message">Total Score</th>
              <th className="widget__message" colSpan="2">
                {score} %
              </th>
            </tr>
          </tfoot>
        </table>
      </section>

      <footer className="container">
        <Link className="button button--link" to="/">
          More Questionaires
        </Link>
      </footer>
    </Fragment>
  );
};

window.Feedback = Feedback;
