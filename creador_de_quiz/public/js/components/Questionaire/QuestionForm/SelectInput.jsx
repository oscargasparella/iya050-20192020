const { React } = window;

const SelectInput = ({ value, onChange, children, ...props }) => (
  <React.Fragment>
    <select
      className="widget-form__input"
      value={value}
      onChange={onChange}
      {...props}
    >
      {children}
    </select>
  </React.Fragment>
);

window.SelectInput = SelectInput;
