const { React } = window;
const { BrowserRouter, Switch, Route, Redirect } = window.ReactRouterDOM;
const { Questionaire } = window;

const QUESTIONAIRE_ID = 1;

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/questionaire/:id" component={Questionaire} />

      <Route exact path="/">
        <Redirect to={`/questionaire/${QUESTIONAIRE_ID}`} />
      </Route>
    </Switch>
  </BrowserRouter>
);

window.App = App;
